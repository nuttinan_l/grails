package myapp

import grails.gorm.transactions.Transactional
import grails.util.Holders
import org.springframework.web.multipart.MultipartFile

@Transactional
class ProductService {

    Product get(long id) {
        return Product.get(id)
    }

    List<Product> list(Map args) {
        return Product.list()
    }

    Long count() {
        return Product.count();
    }

    void delete(long id) {
        Product product = get(id)

        if(product == null){
            return
        }

        product.delete()
    }

    Product save(Product product) {
        if(product == null){
            return null
        }

        product.save()
    }

    Product delete(Product product) {
        if(product == null){
            return null
        }

        product.delete()
    }

    def uploadImage(MultipartFile file){
        if (file.originalFilename){
            String ImagePath = "${getRootPath()}product-image/"
            makeDirectory(ImagePath)
            file.transferTo(new File(ImagePath, file.originalFilename))
            return file.originalFilename
        }
        return ""
    }

    File makeDirectory(String path){
        File file = new File(path)
        if (!file.exists()){
            file.mkdirs()
        }
        return file
    }

    String getRootPath(){
        return Holders.servletContext?.getRealPath("")
    }
}
