package myapp

class Product {

    String name
    Float price
    Date start_date
    String type
    String status
    String image

    static constraints = {
        name maxSize: 255
        type inList: ['Product', 'Service', 'Giveaway']
        status inList: ['Yes', 'No']
        image nullable: true
    }

    String toString() {
        name
    }

}
