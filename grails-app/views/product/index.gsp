<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<div>
    <h1>Products</h1>
    <g:link action="create" resource="${product}"><button>Create</button></g:link>
    <table style="border: 1px solid black">
        <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Price</td>
            <td>Start Date</td>
            <td>Product Type</td>
            <td>Status</td>
            <td>Image</td>
            <td>Active</td>
        </tr>
        </thead>
        <tbody>
        <g:each var="product" in="${productList}">
            <tr>
                <td>${product.id}</td>
                <td>${product.name}</td>
                <td>${product.price}</td>
                <td>${product.start_date}</td>
                <td>${product.type}</td>
                <td>${product.status}</td>
                <td><g:if test="${product?.image}">
                    <img src="${resource(dir: "product-image", file: "/${product.image}")}" class="img-thumbnail" style="margin-top: 10px; height: 100px; width: 100px;"/>
                </g:if></td>
                <td>
                    <g:link action="show" resource="${product}"><button>View</button></g:link>
                    <g:form action="delete" method="DELETE" id="${product.id}">
                        <button>Delete</button>
                    </g:form>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${productCount ?: 0}" />
    </div>
</div>
</body>
</html>