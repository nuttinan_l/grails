package myapp
import groovy.transform.CompileStatic

@CompileStatic
class BootStrap {

    ProductService productService

    def init = { servletContext ->
//        productService.save(new Product([name:"Test 1",price:123.50,start_date:"2021-01-01",type:"Product",status:"Yes"]))
//        productService.save(new Product([name:"Test 2",price:444.50,start_date:"2021-02-01",type:"Service",status:"No"]))
//        productService.save(new Product([name:"Test 3",price:264.00,start_date:"2021-03-01",type:"Giveaway",status:"Yes"]))
//        productService.save(new Product([name:"Test 4",price:1005.50,start_date:"2021-04-01",type:"Product",status:"No"]))
//        productService.save(new Product([name:"Test 5",price:6700.50,start_date:"2021-05-01",type:"Service",status:"Yes"]))
//        productService.save(new Product([name:"Test 6",price:999.50,start_date:"2021-06-01",type:"Giveaway",status:"No"]))
    }
    def destroy = {
    }
}
