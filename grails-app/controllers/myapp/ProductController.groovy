package myapp

import grails.validation.ValidationException
import org.springframework.web.multipart.MultipartFile
import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

@Secured(['permitAll'])
class ProductController {

    ProductService productService

    def index(Integer max) {
        params.max = Math.min(max ?: 1, 5)
        render view:"index",model:[productList:productService.list(),productCount:max]
    }

    def show(Long id) {
        // respond productService.get(id)
        render view:"show",model:[product:productService.get(id)]
    }

    def create() {
        // respond new Product(params)
        render view:"create",model:[product:new Product(params)]
    }

    def save(Product product) {
        if (product == null) {
            notFound()
            return
        }

        try {
            MultipartFile file = params.productImage
            product.image = productService.uploadImage(file)
            productService.save(product)
        } catch (ValidationException e) {
            render view:"create",model:[product:new Product(params)]
        }

        // flash.message = message(code: 'default.created.message', args: [message(code: 'product.label', default: 'Product'), product.id])
        redirect action:"show",params:[id:product.id]

    }

    def edit(Long id) {
        respond productService.get(id)
    }

    def update(Product product) {
        if (product == null) {
            notFound()
            return
        }

        try {
            MultipartFile file = params.productImage
            product.image = productService.uploadImage(file)
            productService.save(product)
        } catch (ValidationException e) {
            respond product.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect product
            }
            '*'{ respond product, [status: OK] }
        }
    }

    def delete(Product product) {
        if (product == null) {
            notFound()
            return
        }

        productService.delete(product)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'product.label', default: 'Product'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
